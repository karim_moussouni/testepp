<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class AjaxController extends Controller
{
    /**
     * @param Request $request
     * @return array
     */
    public function quizAction(Request $request)
    {
        $quiz = $this
            ->getDoctrine()
            ->getRepository("AppBundle:Quiz")
            ->findBy(array("categorie" => $request->get("categorie")));

        return new Response($quiz);
    }
}
