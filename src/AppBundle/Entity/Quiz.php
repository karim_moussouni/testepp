<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Quiz
 *
 * @ORM\Table(name="quiz")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\QuizRepository")
 */
class Quiz
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="titre", type="string", length=255)
     */
    private $titre;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User", inversedBy="id")
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Categorie", inversedBy="id")
     */
    private $category;

    /**
     * @var string
     *
     * @ORM\Column(name="question1", type="string", length=1000)
     */
    private $question1;

    /**
     * @var string
     *
     * @ORM\Column(name="reponse1", type="string", length=1000)
     */
    private $reponse1;
    /**
     * @var string
     *
     * @ORM\Column(name="question2", type="string", length=1000)
     */
    private $question2;

    /**
     * @var string
     *
     * @ORM\Column(name="reponse2", type="string", length=1000)
     */
    private $reponse2;
    /**
     * @var string
     *
     * @ORM\Column(name="question3", type="string", length=1000)
     */
    private $question3;

    /**
     * @var string
     *
     * @ORM\Column(name="reponse3", type="string", length=1000)
     */
    private $reponse3;
    /**
     * @var string
     *
     * @ORM\Column(name="question4", type="string", length=1000)
     */
    private $question4;

    /**
     * @var string
     *
     * @ORM\Column(name="reponse4", type="string", length=1000)
     */
    private $reponse4;
    /**
     * @var string
     *
     * @ORM\Column(name="question5", type="string", length=1000)
     */
    private $question5;

    /**
     * @var string
     *
     * @ORM\Column(name="reponse5", type="string", length=1000)
     */
    private $reponse5;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set titre
     *
     * @param string $titre
     *
     * @return Quiz
     */
    public function setTitre($titre)
    {
        $this->titre = $titre;

        return $this;
    }

    /**
     * Get titre
     *
     * @return string
     */
    public function getTitre()
    {
        return $this->titre;
    }

    /**
     * @return mixed
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @param mixed $category
     * @return Quiz
     */
    public function setCategory($category)
    {
        $this->category = $category;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getQuestions()
    {
        return $this->questions;
    }

    /**
     * @param mixed $questions
     * @return Quiz
     */
    public function setQuestions($questions)
    {
        $this->questions = $questions;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     * @return Quiz
     */
    public function setUser($user)
    {
        $this->user = $user;
        return $this;
    }

    /**
     * @return string
     */
    public function getQuestion1()
    {
        return $this->question1;
    }

    /**
     * @param string $question1
     * @return Quiz
     */
    public function setQuestion1($question1)
    {
        $this->question1 = $question1;
        return $this;
    }

    /**
     * @return string
     */
    public function getReponse1()
    {
        return $this->reponse1;
    }

    /**
     * @param string $reponse1
     * @return Quiz
     */
    public function setReponse1($reponse1)
    {
        $this->reponse1 = $reponse1;
        return $this;
    }

    /**
     * @return string
     */
    public function getQuestion2()
    {
        return $this->question2;
    }

    /**
     * @param string $question2
     * @return Quiz
     */
    public function setQuestion2($question2)
    {
        $this->question2 = $question2;
        return $this;
    }

    /**
     * @return string
     */
    public function getReponse2()
    {
        return $this->reponse2;
    }

    /**
     * @param string $reponse2
     * @return Quiz
     */
    public function setReponse2($reponse2)
    {
        $this->reponse2 = $reponse2;
        return $this;
    }

    /**
     * @return string
     */
    public function getQuestion3()
    {
        return $this->question3;
    }

    /**
     * @param string $question3
     * @return Quiz
     */
    public function setQuestion3($question3)
    {
        $this->question3 = $question3;
        return $this;
    }

    /**
     * @return string
     */
    public function getReponse3()
    {
        return $this->reponse3;
    }

    /**
     * @param string $reponse3
     * @return Quiz
     */
    public function setReponse3($reponse3)
    {
        $this->reponse3 = $reponse3;
        return $this;
    }

    /**
     * @return string
     */
    public function getQuestion4()
    {
        return $this->question4;
    }

    /**
     * @param string $question4
     * @return Quiz
     */
    public function setQuestion4($question4)
    {
        $this->question4 = $question4;
        return $this;
    }

    /**
     * @return string
     */
    public function getReponse4()
    {
        return $this->reponse4;
    }

    /**
     * @param string $reponse4
     * @return Quiz
     */
    public function setReponse4($reponse4)
    {
        $this->reponse4 = $reponse4;
        return $this;
    }

    /**
     * @return string
     */
    public function getQuestion5()
    {
        return $this->question5;
    }

    /**
     * @param string $question5
     * @return Quiz
     */
    public function setQuestion5($question5)
    {
        $this->question5 = $question5;
        return $this;
    }

    /**
     * @return string
     */
    public function getReponse5()
    {
        return $this->reponse5;
    }

    /**
     * @param string $reponse5
     * @return Quiz
     */
    public function setReponse5($reponse5)
    {
        $this->reponse5 = $reponse5;
        return $this;
    }
}

