ePressPack Quiz
===============

##Requirements:
php5.6/7
composer
yarn
npm/node

##Install
composer.phar install

php bin/console doctrine:database:create
php bin/console doctrine:schema:update --force


php bin/console fos:user:create admin mouss38@gmail.com 1234
php bin/console fos:user:promote admin ROLE_ADMIN

php bin/console ass:ins --symlink
yarn 

./node_modules/.bin/encore dev

##Access
Back: /admin